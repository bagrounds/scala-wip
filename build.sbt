lazy val root = (project in file("."))
  .settings(
    inThisBuild(
      List(
        organization := "org.bagrounds",
        scalaVersion := "2.12.4",
        version := "0.1.0"
      )
    ),
    name := "fun-scalar",
    scalaSource in Compile := baseDirectory.value / "src",
    scalaSource in Test := baseDirectory.value / "test",
    libraryDependencies ++= Seq(
      "org.scalatest" %% "scalatest" % "3.0.4" % "test"
    ),
    resolvers ++= Seq(
      "Artima Maven Repository" at "http://repo.artima.com/releases"
    )
  )

