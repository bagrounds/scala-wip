package org.bagrounds

object FunScalar {
  def abs[T](a: T)(implicit n: Numeric[T]): T = n.abs(a)

  def neg[T](a: T)(implicit n: Numeric[T]): T = n.negate(a)

  def add[T](a: T)(b: T)(implicit n: Numeric[T]): T = n.plus(a, b)

  def sub[T](a: T)(b: T)(implicit n: Numeric[T]): T = n.minus(b, a)

  def mul[T](a: T)(b: T)(implicit n: Numeric[T]): T = n.times(a, b)
}

sealed trait FunTest
case class FunTestD[I, S](
  description: String,
  predicate: I => S => Boolean,
  input: I,
  subject: S
) extends FunTest

object FunTest {
  def test[I, S](predicate: I => S => Boolean)(input: I)
    (subject: S): Boolean = predicate(input)(subject)

  def run[I, S](description: String)(test: I => S => Boolean)
    (input: I)(subject: S): Either[String, String] =
    if (!test(input)(subject)) Left(s"not ok - $description")
    else Right(s"ok - $description")

  def idFor[I](i: I)(f: I => I): Boolean = f(i) == i

  def run[I, S](t: FunTestD[I, S]): Either[String, String] = t match {
    case t: FunTestD[I, S] =>
      run(t.description)(t.predicate)(t.input)(t.subject)
  }
}

object FunVector {
  def unary[T, S](f: T => S)(a: Iterable[T]): Iterable[S] = a map f

  def abs[T: Numeric](a: Iterable[T]): Iterable[T] =
    unary[T, T](FunScalar.abs[T])(a)
  def neg[T: Numeric](a: Iterable[T]): Iterable[T] =
    unary[T, T](FunScalar.neg[T])(a)

  def binary[T, S, U](f: T => S => U)(a: Iterable[T])
    (b: Iterable[S]): Iterable[U] =
    (a zip b) map { case (a, b) => f(a)(b) }

  def add[T: Numeric](a: Iterable[T])(b: Iterable[T]): Iterable[T] =
    binary[T, T, T](FunScalar.add[T])(a)(b)

  def sub[T: Numeric](a: Iterable[T])(b: Iterable[T]): Iterable[T] =
    binary[T, T, T](FunScalar.sub[T])(a)(b)

  def mul[T: Numeric](a: Iterable[T])(b: Iterable[T]): Iterable[T] =
    binary[T, T, T](FunScalar.mul[T])(a)(b)
}
