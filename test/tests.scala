import org.bagrounds.FunScalar.{
  abs,
  neg,
  add,
  sub,
  mul
}

import org.bagrounds.FunVector.{
  abs => vabs,
  neg => vneg,
  add => vadd,
  sub => vsub,
  mul => vmul,
  unary,
  binary
}

import org.bagrounds.FunTest
import org.bagrounds.FunTestD

import org.scalatest._

class ScalarTests extends FunSuite {
  def equalFor[A, B](f: A => B)(g: A => B)(a: A): Boolean = f(a) === g(a)
  def idWith[I](f: I => I)(i: I): Boolean = f(i) === i
  def id[X](x: X) = x
  def k[A, B](a: A)(b: B): A = a
  def idFor[Y]: (Y => Y) => Y => Boolean = equalFor[Y, Y](id)

  val nonNeg = Seq(0, 1, 10, 100, 1000)
  val nonNegDoubles = Seq(0.0, 1.0, 10.0, 100.0, 1000.0)
  val negs = Seq(-1, -10, -100, -1000)
  val all = nonNeg ++ negs
  def addInverse(x: Int) = add(x)(neg(x))
  def b[A, B, C](f: B => C)(g: A => B)(a: A): C = f(g(a))

  Seq(
    (
      "for nonNeg, abs = id",
      nonNeg.forall(idFor[Int](abs))
    ),
    (
      "for nonNegDoubles, abs = id",
      nonNegDoubles.forall(idFor(abs[Double]))
    ),
    (
      "for negs, (neg . abs) = id",
      negs.forall(idFor(b(neg[Int])(abs[Int])))
    ),
    (
      "for all, (neg . neg) = id",
      all.forall(idFor(b(neg[Int])(neg[Int])))
    ),
    (
      "for all, (add 0) = id",
      all.forall(idFor(add(0)))
    ),
    (
      "for all x, add x -x = 0",
      all.forall(equalFor(addInverse)(k(0)))
    )
  ).foreach(t => test(t._1)(assert(t._2)))

  Seq(
    FunTestD[Int, Int => Int](
      "add(1)(x) == x",
      FunTest.idFor,
      1,
      add(1)
    ),
    FunTestD[Int, Int => Int](
      "add(0)(x) == x",
      FunTest.idFor,
      1,
      add(0)
    )
  ).map(FunTest.run[Int, Int => Int])
    .map(_.fold(id, id))
    .foreach(println)

  test("add") {
    assert(add(3)(4) === 7)
    assert(add(-3)(4) === 1)
    assert(add(3)(-4) === -1)
    assert(add(-3)(-4) === -7)
  }
  test("vabs") {
    assert(vabs[Int](Seq(-3, 4, 5)) === Seq(3, 4, 5))
  }
  test("vadd") {
    assert(vadd[Int](Seq(3, 4, 5))(Seq(1, 2, 3)) === Seq(4, 6, 8))
  }
  test("vsub") {
    assert(
      binary(sub[Int])(Seq(3, 4, 5))(Seq(1, 2, 3)) === Seq(-2, -2, -2)
    )
  }
  test("sub") {
    assert(sub(3)(4) === 1)
    assert(sub(-3)(4) === 7)
    assert(sub(3)(-4) === -7)
    assert(sub(-3)(-4) === -1)
  }
  test("mul") {
    assert(mul(3)(4) === 12)
    assert(mul(-3)(4) === -12)
    assert(mul(3)(-4) === -12)
    assert(mul(-3)(-4) === 12)
  }
}
